package com.serragnoli

import cats.effect.{IO, IOApp}
import fs2.{Stream, text}
import fs2.io.file.{Files, Path}
import zio.{Scope, ZIOAppArgs}

import java.io.File
import scala.io.Source

object common {
  val pathToSource = Path("/Users/fse05/Development/lab/effects/fs2/target/scala-2.13/classes/testdata/fahrenheit.txt")
  val pathToDest   = Path("/Users/fse05/Development/lab/effects/fs2/target/scala-2.13/classes/testdata/celsius.txt")
  val pathToDest2   = Path("/Users/fse05/Development/lab/effects/fs2/target/scala-2.13/classes/testdata/celsius2.txt")

  val fromLineToOutput: String => String       = line => s"$line°F -> ${fahrenheitToCelsius(line.toDouble).toString}°C"
  val ignoreBlankAndComment: String => Boolean = s => !s.trim.isEmpty && !s.startsWith("//")

  private def fahrenheitToCelsius(f: Double): Double = (f - 32.0) * (5.0 / 9.0)
}

object Converter extends IOApp.Simple {
  import common._

  private def converter: Stream[IO, Unit] = {
    Files[IO]
      .readUtf8Lines(pathToSource)
      .filter(ignoreBlankAndComment)
      .map(fromLineToOutput)
      .intersperse("\n")
      .through(text.utf8.encode)
      .through(Files[IO].writeAll(pathToDest))
  }

  override def run: IO[Unit] = converter.compile.drain
}

object ConverterZio extends zio.interop.catz.CatsApp {
  import zio.{Task, UIO, ZIO}
  import zio.interop.catz._
  import common._

  private val converter: Stream[Task, Unit] =
    Files[Task]
      .readUtf8Lines(pathToSource)
      .filter(ignoreBlankAndComment)
      .map(fromLineToOutput)
      .intersperse("\n")
      .through(text.utf8.encode)
      .through(Files[Task].writeAll(pathToDest))

  private val converter2: Stream[Task, Unit] =
    Files[Task]
      .readUtf8Lines(pathToSource)
      .filter(ignoreBlankAndComment)
      .map(fromLineToOutput)
      .intersperse("\n")
      .through(text.utf8.encode)
      .through(Files[Task].writeAll(pathToDest2))

  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = converter.merge(converter2).compile.drain
}
