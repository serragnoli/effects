import Dependencies._

ThisBuild / scalaVersion := "2.13.10"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.serragnoli.effects"
ThisBuild / organizationName := "serragnoli"

lazy val root = (project in file("."))
  .settings(
    name := "effects"
  )
  .aggregate(zio, fs2, cats)

lazy val zio = (project in file("zio")).settings(
  libraryDependencies ++= zioDeps
)

lazy val fs2 = (project in file("fs2")).settings(
  libraryDependencies ++= fs2Deps
)
lazy val cats = (project in file("cats"))
  .settings(
    libraryDependencies ++= catsEffect
  )
