import sbt._

object Dependencies {

  lazy val zioDeps = Seq(
    "dev.zio" %% "zio"         % "2.0.5",
    "dev.zio" %% "zio-streams" % "2.0.5"
  )

  lazy val fs2Deps = typeLevel ++ zio

  private lazy val typeLevel: Seq[ModuleID] = Seq(
    "fs2-core",
    "fs2-io"
  ).map("co.fs2" %% _ % "3.5.0")

  private lazy val zio: Seq[ModuleID] = Seq(
    "dev.zio" %% "zio" % "2.0.6",
    "dev.zio" %% "zio-interop-cats" % "3.3.0"
  )

  lazy val catsEffect: Seq[ModuleID] = Seq(
    "org.typelevel" %% "cats-effect" % "3.4.8"
  )
}
