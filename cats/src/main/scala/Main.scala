import cats.data.{EitherT, OptionT}
import cats.syntax.option._
import cats.syntax.list._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.Future
import scala.util.Try

object Main extends App {

//  val list: List[Int] = (1 to 10).toList
//
//  val eitherT: EitherT[List, String, Int]       = EitherT.right(List(7))
//  val either: List[Either[String, Int]] = List(Right(7))
//
//  println(
//    ">>> " + eitherT.map(_ * 2)
//  )
//  println(
//    ">>> " + either.map(_.map(_ * 2))
//  )

  private def parseDouble(s: String): Either[String, Double] =
    (Try(s.toDouble).map(Right(_)).getOrElse(Left(s"$s is not a number")))

  private def divide(a: Double, b: Double): Either[String, Double] =
    Either.cond(b != 0, a / b, "Cannot divide by zero")

  private def divisionProgram(inputA: String, inputB: String): Either[String, Double] = {
    for {
      a      <- parseDouble(inputA)
      b      <- parseDouble(inputB)
      result <- divide(a, b)
    } yield result
  }

  //  ----------------------------------------------------

  private def parseDoubleAsync(s: String): Future[Either[String, Double]] =
    Future(parseDouble(s))

  private def divideAsync(a: Double, b: Double): Future[Either[String, Double]] =
    Future(divide(a, b))

  private def divisionProgramAsync(inputA: String, inputB: String): Future[Either[String, Double]] =
    parseDoubleAsync(inputA).flatMap { eitherA =>
      parseDoubleAsync(inputB).flatMap { eitherB =>
        (eitherA, eitherB) match {
          case (Right(a), Right(b)) => divideAsync(a, b)
          case (Left(err), _)       => Future.successful(Left(err))
          case (_, Left(err))       => Future.successful(Left(err))
        }
      }
    }

  //  ----------------------------------------------------

  private def divisionProgramF(inputA: String, inputB: String): EitherT[Future, String, Double] =
    for {
      a <- EitherT(parseDoubleAsync(inputA))
      b <- EitherT(parseDoubleAsync(inputB))
      result <- EitherT(divideAsync(a, b))
    } yield result


  //  ----------------------------------------------------

  println(
    ">>> Simple: " + divisionProgram("6", "2")
  )
  println(
    ">>> Async: " + divisionProgramAsync("16", "4").foreach(println)
  )
  println(
    ">>> EitherT: " + divisionProgramF("27", "3").value.foreach(println)
  )

  val x: List[Option[Int]] = (1 to 10).toList.map(_.some)
  println(s">>> x: ${x.foldLeft(0)((i, o) => i + o.product)}")

  val y: OptionT[List, Int] = OptionT(x)
  println(s">>> y: ${y.foldLeft(0)(_ + _)}")

}
