import cats.effect.{IO, IOApp}
import scala.concurrent.duration._

object MainIO extends IOApp.Simple {

  val loop: IO[Unit] = IO.println("Hello, World!") >> loop

  override def run: IO[Unit] = loop.timeout(5.seconds)
}
