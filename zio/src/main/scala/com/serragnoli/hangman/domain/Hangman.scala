package com.serragnoli.hangman.domain

import zio._

import java.io.IOException

object Hangman extends ZIOAppDefault {
  override def run: IO[IOException, Unit] =
    for {
      _    <- Console.printLine("Welcome to ZIO Hangman")
      name <- getName
      word <- chooseWord
      _    <- gameLoop(State.initial(name, word))
    } yield ()

  private def getUserInput(message: String): IO[IOException, String] = Console.readLine(message)

  lazy val getName: IO[IOException, Name] = for {
    input <- getUserInput("What's your name? ")
//    name <- Name.make(input).fold(Console.printLine("Invalid input. Please try again") *> getName)(ZIO.succeed(_))
    name <- ZIO.from(Name.make(input)) <> (Console.printLine("Invalid input. Please try again") *> getName)
  } yield name

  lazy val chooseWord: UIO[Word] =
    for {
      index <- Random.nextIntBounded(words.length)
      word  <- ZIO.fromOption(words.lift(index).flatMap(Word.make)).orDieWith(_ => new Error("Boom!"))
    } yield word

  def renderState(state: State): IO[IOException, Unit] = {
    /*
          --------
          |      |
          |      0
          |     \|/
          |      |
          |     / \
          -

          f     n  c  t  o
          -  -  -  -  -  -  -
          Guesses: a, z, y, x
     */

    val hangman = ZIO.attempt(hangmanStages(state.failuresCount)).orDie
    val word = state.wordToGuess.toList
      .map(c => if (state.guesses.map(_.char).contains(c)) s" $c " else "   ")
      .mkString
    val line    = List.fill(state.wordToGuess.length)(" - ").mkString
    val guesses = s"Guesses: ${state.guesses.map(_.char).mkString(", ")}"

    hangman.flatMap { hangman =>
      Console.printLine {
        s"""
          |$hangman
          |
          |$word
          |$line
          |
          |$guesses
          |
          |
          |""".stripMargin
      }
    }
  }

  lazy val getGuess: IO[IOException, Guess] = {
    for {
      input <- getUserInput("Type the letter of your guess:")
      guess <- ZIO
        .fromOption(Guess.make(input)) <> (Console.printLine("Invalid input. Please try again...") *> getGuess)
    } yield guess
  }

  def analyseNewGuess(oldState: State, newState: State, guess: Guess): GuessResult = (oldState, newState) match {
    case (oldS, _) if oldS.guesses.contains(guess)          => GuessResult.Unchanged
    case (_, newS) if newS.playerWon                        => GuessResult.Won
    case (_, newS) if newS.playerLost                       => GuessResult.Lost
    case (oldS, _) if oldS.wordToGuess.contains(guess.char) => GuessResult.Correct
    case _                                                  => GuessResult.Incorrect
  }

  def gameLoop(oldState: State): IO[IOException, Unit] =
    for {
      guess <- renderState(oldState) <*> getGuess
      newState    = oldState.addGuesses(guess)
      guessResult = analyseNewGuess(oldState, newState, guess)
      _ <- guessResult match {
        case GuessResult.Won =>
          Console.printLine(s"Congratulations ${newState.player.name}! You won!") <*> renderState(newState)
        case GuessResult.Lost =>
          Console.printLine(s"Sorry ${newState.player.name}! You Lost! Word was: ${newState.wordToGuess.word}") <*>
            renderState(newState)
        case GuessResult.Correct =>
          Console.printLine(s"Good guess, ${newState.player.name}!") <*> gameLoop(newState)
        case GuessResult.Incorrect =>
          Console.printLine(s"Bad guess, ${newState.player.name}!") <*> gameLoop(newState)
        case GuessResult.Unchanged =>
          Console.printLine(s"${newState.player.name}, You've already tried that letter!") <*> gameLoop(newState)
      }
    } yield ()
}
