package com.serragnoli.hangman

package object domain {
  // "abstract = No `copy` method
  sealed abstract case class Name private (name: String)
  object Name {
    def make(name: String): Option[Name] = if (name.isEmpty) None else Some(new Name(name) {})
  }

  sealed abstract case class Guess private (char: Char)
  object Guess {
    def make(str: String): Option[Guess] =
      Some(str.toList).collect {
        case c :: Nil if c.isLetter => new Guess(c.toLower) {}
      }
  }

  sealed abstract case class Word private (word: String) {
    def contains(char: Char): Boolean = word.contains(char)
    val length: Int                   = word.length
    def toList: List[Char]            = word.toList
    def toSet: Set[Char]              = word.toSet
  }
  object Word {
    def make(word: String): Option[Word] =
      if (word.nonEmpty && word.forall(_.isLetter)) Some(new Word(word) {}) else None
  }

  sealed abstract case class State private (player: Name, guesses: Set[Guess], wordToGuess: Word) {
    def addGuesses(guess: Guess): State = new State(player, guesses.incl(guess), wordToGuess) {}
    def playerLost: Boolean             = failuresCount > 5
    def playerWon: Boolean              = (wordToGuess.toSet.removedAll(guesses.map(_.char))).isEmpty
    def failuresCount: Int              = (guesses.map(_.char).removedAll(wordToGuess.toSet)).size
  }
  object State {
    def initial(player: Name, wordToGuess: Word): State = new State(player, Set.empty, wordToGuess) {}
  }

  sealed trait GuessResult
  object GuessResult {
    case object Won       extends GuessResult
    case object Lost      extends GuessResult
    case object Correct   extends GuessResult
    case object Incorrect extends GuessResult
    case object Unchanged extends GuessResult
  }

  val words: List[String] = List(
    "Abjure",
    "Bandit",
    "Board",
    "Camera",
    "Campus",
    "Celery",
    "Damage",
    "Dark",
    "Eulogy",
    "Fish",
    "Food",
    "Future",
    "Goofy",
    "Invest",
    "Law",
    "Life",
    "Love",
    "Man",
    "Mascot",
    "Mine",
    "Near",
    "Owl",
    "Phone",
    "Picnic",
    "Ring",
    "Ship",
    "Tennis",
    "Tree",
    "War",
    "Way",
    "Wealth",
    "World",
    "Xerox",
    "You"
  ).map(_.toLowerCase)

  val hangmanStages: List[String] = List(
"""
     *            --------
     *            |      |
     *            |
     *            |
     *            |
     *            |
     *            -
""",
"""
     *            --------
     *            |      |
     *            |      0
     *            |
     *            |
     *            |
     *            -
""",
    """
      *          --------
      *          |      |
      *          |      0
      *          |      |
      *          |      |
      *          |
      *          -
      *
      *
      *""",
    """
      *          --------
      *          |      |
      *          |      0
      *          |     \|/
      *          |      |
      *          |
      *          -
      *
      *
      *""",
    """
      *          --------
      *          |      |
      *          |      0
      *          |     \|/
      *          |      |
      *          |     /
      *          -
      *
      *
      *""",
    """
      *          --------
      *          |      |
      *          |      0
      *          |     \|/
      *          |      |
      *          |     / \
      *          -
      *
      *
      *""",
  )
}
